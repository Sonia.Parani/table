function openDetails() {
  var showInfo = document.getElementsByClassName("details");

  for (var i = 0; i < showInfo.length; i++) {
    var item = showInfo.item(i);
    if (item.style.display === "none") {
      item.style.display = "table-row";
    } else {
      item.style.display = "none";
    }
  }
}
